package org.opencv.samples.colorblobdetect;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.util.Log;

public class ColorBlobDetector {
	MatOfInt convexHullMatOfInt = new MatOfInt();
	ArrayList<Point> convexHullPointArrayList = new ArrayList<Point>();
	MatOfPoint convexHullMatOfPoint = new MatOfPoint();
	ArrayList<MatOfPoint> convexHullMatOfPointArrayList = new ArrayList<MatOfPoint>();
    // Lower and Upper bounds for range checking in HSV color space
    private Scalar mLowerBound = new Scalar(0);
    private Scalar mUpperBound = new Scalar(0);
    // Minimum contour area in percent for contours filtering
    private static double mMinContourArea = 0.1;
    // Color radius for range checking in HSV color space
    private Scalar mColorRadius = new Scalar(25,50,50,0);
    private Mat mSpectrum = new Mat();
    private List<MatOfPoint> mContours = new ArrayList<MatOfPoint>();

    // Cache
    Mat mPyrDownMat = new Mat();
    Mat mHsvMat = new Mat();
    Mat mMask = new Mat();
    Mat mDilatedMask = new Mat();
    Mat mHierarchy = new Mat();

    public void setColorRadius(Scalar radius) {
        mColorRadius = radius;
    }

    public void setHsvColor(
    		//Scalar hsvColor
    		) {
        //double minH = (hsvColor.val[0] >= mColorRadius.val[0]) ? hsvColor.val[0]-mColorRadius.val[0] : 0;
       // double maxH = (hsvColor.val[0]+mColorRadius.val[0] <= 255) ? hsvColor.val[0]+mColorRadius.val[0] : 255;

        /*mLowerBound.val[0] = minH;
        mUpperBound.val[0] = maxH;

        mLowerBound.val[1] = hsvColor.val[1] - mColorRadius.val[1];
        mUpperBound.val[1] = hsvColor.val[1] + mColorRadius.val[1];

        mLowerBound.val[2] = hsvColor.val[2] - mColorRadius.val[2];
        mUpperBound.val[2] = hsvColor.val[2] + mColorRadius.val[2];

        mLowerBound.val[3] = 0;
        mUpperBound.val[3] = 255;*/
        
        mLowerBound.val[0] = 0.0;
        mUpperBound.val[0] = 42;

        mLowerBound.val[1] = 35.40625;
        mUpperBound.val[1] = 153.15625;

        mLowerBound.val[2] = 152.296875;
        mUpperBound.val[2] = 285.328125;

        mLowerBound.val[3] = 0;
        mUpperBound.val[3] = 255;
        
        Log.d("fixed_color","mLowerBound.val[0]:"+mLowerBound.val[0]);
        Log.d("fixed_color","mUpperBound.val[0]:"+mUpperBound.val[0]);
        
        Log.d("fixed_color","mLowerBound.val[1]:"+mLowerBound.val[1]);
        Log.d("fixed_color","mUpperBound.val[1]:"+mUpperBound.val[1]);
        
        Log.d("fixed_color","mLowerBound.val[2]:"+mLowerBound.val[2]);
        Log.d("fixed_color","mUpperBound.val[2]:"+mUpperBound.val[2]);
        
        //Log.d("fixed_color","maxH-minH:"+(maxH-minH)+" minH:"+minH);
        Mat spectrumHsv = new Mat(1, (int)42, CvType.CV_8UC3);

        for (int j = 0; j < 42; j++) {
            byte[] tmp = {(byte)(j), (byte)255, (byte)255};
            spectrumHsv.put(0, j, tmp);
        }

        Imgproc.cvtColor(spectrumHsv, mSpectrum, Imgproc.COLOR_HSV2RGB_FULL, 4);
    }

    public Mat getSpectrum() {
        return mSpectrum;
    }

    public void setMinContourArea(double area) {
        mMinContourArea = area;
    }

    public void process(Mat rgbaImage) {
        Imgproc.pyrDown(rgbaImage, mPyrDownMat);
        Imgproc.pyrDown(mPyrDownMat, mPyrDownMat);

        Imgproc.cvtColor(mPyrDownMat, mHsvMat, Imgproc.COLOR_RGB2HSV_FULL);

        Core.inRange(mHsvMat, mLowerBound, mUpperBound, mMask);
        Imgproc.dilate(mMask, mDilatedMask, new Mat());

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(mDilatedMask, contours, mHierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        // Find max contour area
        double maxArea = 0;
        Iterator<MatOfPoint> each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }

        // Filter contours by area and resize to fit the original image size
        mContours.clear();
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mContours.add(contour);
            }
        }
    }
    public void convexHullProcess(Mat rgbaImage) {
    	
       Imgproc.pyrDown(rgbaImage, mPyrDownMat);
       Imgproc.pyrDown(mPyrDownMat, mPyrDownMat);
       Imgproc.cvtColor(mPyrDownMat, mHsvMat, Imgproc.COLOR_RGB2GRAY);
       Imgproc.dilate(mHsvMat, mHsvMat, new Mat());
       Imgproc.Canny(mHsvMat, mHsvMat, 80, 90);
       Imgproc.GaussianBlur(mHsvMat, mHsvMat, new Size(5, 5), 5);
       

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(mHsvMat, contours, mHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        // Find max contour area
        double maxArea = 0;
        if(contours.size()==0){
        	return;
        }
      
        Iterator<MatOfPoint> each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea){
                maxArea = area;
                
                }
        }

        // Filter contours by area and resize to fit the original image size
        mContours.clear();
       // Core.multiply(biggestContour, new Scalar(4,4), biggestContour);
       // mContours.add(biggestContour);
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) >= mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mContours.add(contour);
            }
        }
    }
    public void calculateConvexHulls(List<MatOfPoint> aproximatedContours)
    {
        

        try {
            //Calculate convex hulls
            if(aproximatedContours.size() > 0)
            {
                Imgproc.convexHull( aproximatedContours.get(0), convexHullMatOfInt, false);

                for(int j=0; j < convexHullMatOfInt.toList().size(); j++)
                    convexHullPointArrayList.add(aproximatedContours.get(0).toList().get(convexHullMatOfInt.toList().get(j)));
                convexHullMatOfPoint.fromList(convexHullPointArrayList);
                convexHullMatOfPointArrayList.add(convexHullMatOfPoint);    
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("Calculate convex hulls failed.", "Details below");
            e.printStackTrace();
        }
    }
    public List<MatOfPoint> getContours() {
        return mContours;
    }
}
