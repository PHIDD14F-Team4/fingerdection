package org.opencv.samples.colorblobdetect;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.widget.CardBuilder;



public class Main extends Activity /*implements OnInitListener,OnUtteranceCompletedListener*/ {
  /*private static TextToSpeech tts;
  Map<String, String> phoneticMap = new HashMap<String, String>();
  String cleanPhonemes = "";
  private static HashMap<String, String> params;*/
  protected static final String TAG = "MAIN ACTIVITY";
  View cardView;
  private GestureDetector mGestureDetector;
   
  @Override
  protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      mGestureDetector = createGestureDetector(this);
      
      cardView = new CardBuilder(this, CardBuilder.Layout.CAPTION)
      .setText("Reading Helper")
      .setFootnote("Tap to continue")
//      .setTimestamp("just now")
      .getView();
      
      // Display the card we just created
      setContentView(cardView);
  }
  
    @Override
    protected void onResume() {
        // To receive touch events from the touchpad, the view should have focus.
        cardView.requestFocus();
        super.onResume();
    }
/*
    @Override
    protected void onDestroy() {
      if (tts!=null) {
        tts.stop();
        tts.shutdown();
      }
      super.onDestroy();
    }*/
    private GestureDetector createGestureDetector(Context context) {
      GestureDetector gestureDetector = new GestureDetector(context);
          //Create a base listener for generic gestures
          gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
              @Override
              public boolean onGesture(Gesture gesture) {
                  if (gesture == Gesture.TAP) {
                    Intent mIntent = new Intent(Main.this, ColorBlobDetectionActivity.class);        
                    startActivity(mIntent);
                    Log.d(TAG, "Gesture.TAP");
                      return true;
                  } else if (gesture == Gesture.TWO_TAP) {
                      // do something on two finger tap
                    Log.d(TAG, "Gesture.TWO_TAP");
                      return true;
                  } else if (gesture == Gesture.SWIPE_RIGHT) {
                      // do something on right (forward) swipe
                    Log.d(TAG, "Gesture.SWIPE_RIGHT");
                      return true;
                  } else if (gesture == Gesture.SWIPE_LEFT) {
                      // do something on left (backwards) swipe
                    Log.d(TAG, "Gesture.SWIPE_LEFT");
                      return true;
                  }
                  return false;
              }
          });
          gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
              @Override
              public void onFingerCountChanged(int previousCount, int currentCount) {
                // do something on finger count changes
                Log.d(TAG, "onFingerCountChanged(), previousCount: " + previousCount 
                    + ", currentCount: " + currentCount);
              }
          });
          gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {
              @Override
              public boolean onScroll(float displacement, float delta, float velocity) {
                  // do something on scrolling
                Log.d(TAG, "onScroll(), displacement: " + displacement 
                    + ", delta: " + delta + ", velocity: " + velocity);
                return true;
              }
          });
          return gestureDetector;
      }

      /*
       * Send generic motion events to the gesture detector
       */
      @Override
      public boolean onGenericMotionEvent(MotionEvent event) {
          if (mGestureDetector != null) {
              return mGestureDetector.onMotionEvent(event);
          }
          return false;
      }
/*
      @Override
      public void onInit(int code) {
        // TODO Auto-generated method stub
        Log.d(TAG, "code: " + code);
        if (code==TextToSpeech.SUCCESS) {
          Log.d(TAG, "Inside code TextToSpeech Success");
          tts.setLanguage(Locale.getDefault());
          tts.setOnUtteranceCompletedListener(this);
          params = new HashMap<String, String>();
        } else {
          tts = null;
          Log.d(TAG, "Failed to initialize TTS engine.");
        }

        try {
          createMap(getAssets().open("cmudict.0.7a.txt"));
        } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      private void createMap(InputStream open) {
        Scanner scanner = new Scanner(open);
        while(scanner.hasNext()) 
        {
          String[] values = scanner.next().split(":");
          phoneticMap.put(values[0], values[1]);
        }
        scanner.close();

      }
      public static void speak(String text) {
        if(text != null) {
          params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
          Log.v("TTS", "TextToSpeech.Engine.KEY_PARAM_STREAM: "+TextToSpeech.Engine.KEY_PARAM_STREAM);
          Log.v("TTS", "String.valueOf(AudioManager.STREAM_ALARM): "+String.valueOf(AudioManager.STREAM_ALARM));
          
          params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,text);
          Log.v("TTS", "TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID: "+TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID);
          Log.v("TTS", "text: "+text);
          
          tts.speak(text, TextToSpeech.QUEUE_ADD, params);
        }
      }
      public void onUtteranceCompleted(String uttId) {
        Log.v("TTS", "onUtteranceCompleted: "+uttId);
//        ((EditText)findViewById(R.id.outputText1)).setText(uttId);
        
      }
      
      private String toIPA(String phoneme){
        if(phoneme.contains("AA")) return "a";
        else if(phoneme.contains("AE")) return "æ";
        else if(phoneme.contains("AH1")) return "ʌ";
        else if(phoneme.contains("AH0")) return "ə";
        else if(phoneme.contains("AO")) return "ɔ";
        else if(phoneme.contains("AW")) return "aʊ";
        else if(phoneme.contains("AY")) return "aɪ";
        else if(phoneme.contains("B")) return "b";
        else if(phoneme.contains("CH")) return "tʃ";
        else if(phoneme.contains("D")) return "d";
        else if(phoneme.contains("DH")) return "ð";
        else if(phoneme.contains("EH")) return "ɛ";
        else if(phoneme.contains("ER")) return "ɝ";
        else if(phoneme.contains("EY")) return "eɪ";
        else if(phoneme.contains("F")) return "f";
        else if(phoneme.contains("G")) return "g";
        else if(phoneme.contains("HH")) return "h";
        else if(phoneme.contains("IH")) return "ɪ";
        else if(phoneme.contains("IY")) return "i";
        else if(phoneme.contains("JH")) return "dʒ";
        else if(phoneme.contains("K")) return "k";
        else if(phoneme.contains("L")) return "ɫ";
        else if(phoneme.contains("M")) return "m";
        else if(phoneme.contains("N")) return "n";
        else if(phoneme.contains("NG")) return "ŋ";
        else if(phoneme.contains("OW")) return "oʊ";
        else if(phoneme.contains("OY")) return "ɔɪ";
        else if(phoneme.contains("P")) return "p";
        else if(phoneme.contains("R")) return "r";
        else if(phoneme.contains("S")) return "s";
        else if(phoneme.contains("SH")) return "ʃ";
        else if(phoneme.contains("T")) return "t";
        else if(phoneme.contains("TH")) return "θ";
        else if(phoneme.contains("UH")) return "ʊ";
        else if(phoneme.contains("UW")) return "u";
        else if(phoneme.contains("V")) return "v";
        else if(phoneme.contains("W")) return "w";
        else if(phoneme.contains("Y")) return "j";
        else if(phoneme.contains("Z")) return "z";
        else if(phoneme.contains("ZH")) return "ʒ";
        else return phoneme;

      }
      
      private String toXSAMPA(String phoneme){
        
        if (phoneme.equals("AA")) return "A";
        else if (phoneme.equals("AA0")) return "A";
        else if (phoneme.equals("AA1")) return "&quot;A";
        else if (phoneme.equals("AA2")) return "A:";
        
        else if (phoneme.equals("AE")) return "&{";
        else if (phoneme.equals("AE0")) return "&{";
        else if (phoneme.equals("AE1")) return "&quot;{";
        else if (phoneme.equals("AE2")) return "{";
        
        else if (phoneme.equals("AH")) return "O";
        else if (phoneme.equals("AH0")) return "@";
        else if (phoneme.equals("AH1")) return "&quot;V";
        else if (phoneme.equals("AH2")) return "V";
        
        else if (phoneme.equals("AO")) return "O";
        else if (phoneme.equals("AO0")) return "O";
        else if (phoneme.equals("AO1")) return "&quot;O";
        else if (phoneme.equals("AO2")) return "O";
        
        else if (phoneme.equals("AW")) return "aU";
        else if (phoneme.equals("AW0")) return "aU";
        else if (phoneme.equals("AW1")) return "&quot;aU";
        else if (phoneme.equals("AW2")) return "aU";
        
        else if (phoneme.equals("AY")) return "aI";
        else if (phoneme.equals("AY0")) return "aI";
        else if (phoneme.equals("AY1")) return "&quot;aI";
        else if (phoneme.equals("AY2")) return "aI";
        
        else if (phoneme.equals("B")) return "b";
        else if (phoneme.equals("CH")) return "tS";
        else if (phoneme.equals("D")) return "d";
        else if (phoneme.equals("DH")) return "D";
        
        else if (phoneme.equals("EH")) return "{";
        else if (phoneme.equals("EH0")) return "{";
        else if (phoneme.equals("EH1")) return "&quot;e{";
        else if (phoneme.equals("EH2")) return "{";
        
        else if (phoneme.equals("ER")) return "@";
        else if (phoneme.equals("ER0")) return "@";
        else if (phoneme.equals("ER1")) return "&quot;3";
        else if (phoneme.equals("ER2")) return "3:";
        
        else if (phoneme.equals("EY")) return "eI";
        else if (phoneme.equals("EY0")) return "eI";
        else if (phoneme.equals("EY1")) return "&quot;eI";
        else if (phoneme.equals("EY2")) return "eI";
        
        else if (phoneme.equals("F")) return "f";
        else if (phoneme.equals("G")) return "g";
        else if (phoneme.equals("HH")) return "h";
        
        else if (phoneme.equals("IH")) return "I";
        else if (phoneme.equals("IH0")) return "i";
        else if (phoneme.equals("IH1")) return "&quot;i";
        else if (phoneme.equals("IH2")) return "i:";
        
        else if (phoneme.equals("IY")) return "I";
        else if (phoneme.equals("IY0")) return "i";
        else if (phoneme.equals("IY1")) return "&quot;I;";
        else if (phoneme.equals("IY2")) return "fi";
        
        else if (phoneme.equals("JH")) return "dZ";
        else if (phoneme.equals("K")) return "k";
        else if (phoneme.equals("L")) return "l";
        else if (phoneme.equals("M")) return "m";
        else if (phoneme.equals("N")) return "n";
        else if (phoneme.equals("NG")) return "N";
        
        else if (phoneme.equals("OW")) return "o";
        else if (phoneme.equals("OW0")) return "o";
        else if (phoneme.equals("OW1")) return "&quot;o";
        else if (phoneme.equals("OW2")) return "o";
        
        else if (phoneme.equals("OY")) return "OI";
        else if (phoneme.equals("OY0")) return "OI";
        else if (phoneme.equals("OY1")) return "&quot;OI";
        else if (phoneme.equals("OY2")) return "OI";
        
        else if (phoneme.equals("P")) return "p";
        else if (phoneme.equals("R")) return "r";
        else if (phoneme.equals("S")) return "s";
        else if (phoneme.equals("SH")) return "S";
        else if (phoneme.equals("T")) return "t";
        else if (phoneme.equals("TH")) return "T";
        
        else if (phoneme.equals("UH")) return "U";
        else if (phoneme.equals("UH0")) return "U";
        else if (phoneme.equals("UH1")) return "&quot;U";
        else if (phoneme.equals("UH2")) return "U";
        
        else if (phoneme.equals("UW")) return "u:";
        else if (phoneme.equals("UW0")) return "u:";
        else if (phoneme.equals("UW1")) return "&quot;u:";
        else if (phoneme.equals("UW2")) return "u:";
        
        else if (phoneme.equals("V")) return "v";
        else if (phoneme.equals("W")) return "w";
        else if (phoneme.equals("Y")) return "y";
        else if (phoneme.equals("Z")) return "z";
        else if (phoneme.equals("ZH")) return "Z";
        else return phoneme;

      }
*/
}
