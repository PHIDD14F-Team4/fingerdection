package org.opencv.samples.colorblobdetect;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgproc.Imgproc;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.googlecode.tesseract.android.TessBaseAPI;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;

public class ColorBlobDetectionActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
	public static int index = 0;
    private static final String  TAG              = "OCVSample::Activity";

    private boolean              mIsColorSelected = false;
    private boolean              mIsImageSelected = false;
    private Mat                  mRgba;
    private Scalar               mBlobColorRgba;
    private Scalar               mBlobColorHsv;
    private ColorBlobDetector    mDetector;
    private Mat                  mSpectrum;
    private Size                 SPECTRUM_SIZE;
    private Scalar               CONTOUR_COLOR;
    private MatOfPoint           box;
    ArrayList<MatOfPoint> listOfBoxes;
    private OGView mOpenCvCameraView;
    private GestureDetector mGestureDetector;
    org.opencv.core.Point boxStartPos;
    int boxStartX,boxStartY,boxEndX,boxEndY;
	org.opencv.core.Point boxEndPos;
    
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    mOpenCvCameraView.enableView();
                    //mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public ColorBlobDetectionActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.color_blob_detection_surface_view);
        mGestureDetector = createGestureDetector(this);
        
        mOpenCvCameraView = (OGView) findViewById(R.id.color_blob_detection_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null){
        	mOpenCvCameraView.releaseCamera();
//            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_5, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null){
        	mOpenCvCameraView.releaseCamera();
//            mOpenCvCameraView.disableView();
        }
    }

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mDetector = new ColorBlobDetector();
        mSpectrum = new Mat();
        mBlobColorRgba = new Scalar(255);
        mBlobColorHsv = new Scalar(255);
        SPECTRUM_SIZE = new Size(200, 64);
        //CONTOUR_COLOR = new Scalar(255,0,0,255);
        CONTOUR_COLOR = new Scalar(0,255,0);
        boxStartX = 245;
        boxStartY = 135;
        boxEndX = 395;
        boxEndY = 185;
        boxStartPos = new org.opencv.core.Point(boxStartX,boxStartY);
        		boxEndPos = new org.opencv.core.Point(boxEndX,boxEndY);
        
        //provide fixed range of skin's color
        mDetector.setHsvColor();
        Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);
        mIsColorSelected = true;
    }

    public void onCameraViewStopped() {
        mRgba.release();
        mOpenCvCameraView.releaseCamera();
    }
 /*   @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_DPAD_CENTER) {
        	Log.d("fingertip_pos","key down");
        	//mIsImageSelected = true;
            return true;
        }
        
        return super.onKeyDown(keycode, event);
    }
    */
    public boolean onTouch(View v, MotionEvent event) {
    	Log.d("fingertip_pos","key down");
    	mIsImageSelected = true;
 /*       int cols = mRgba.cols();
        int rows = mRgba.rows();

        int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2;
        int yOffset = (mOpenCvCameraView.getHeight() - rows) / 2;

        int x = (int)event.getX() - xOffset;
        int y = (int)event.getY() - yOffset;

        Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");

        if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;

        Rect touchedRect = new Rect();

        touchedRect.x = (x>4) ? x-4 : 0;
        touchedRect.y = (y>4) ? y-4 : 0;

        touchedRect.width = (x+4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y+4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);

        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region
        mBlobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width*touchedRect.height;
        for (int i = 0; i < mBlobColorHsv.val.length; i++)
            mBlobColorHsv.val[i] /= pointCount;

        mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);

        Log.i(TAG, "Touched rgba color: (" + mBlobColorRgba.val[0] + ", " + mBlobColorRgba.val[1] +
                ", " + mBlobColorRgba.val[2] + ", " + mBlobColorRgba.val[3] + ")");

       // mDetector.setHsvColor(mBlobColorHsv);

       // Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);

        mIsColorSelected = true;

        touchedRegionRgba.release();
        touchedRegionHsv.release();
*/
        return false; // don't need subsequent touch events
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        //box = new MatOfPoint(new Mat(mRgba, new Range(135, 185), new Range(245,395)));
       // listOfBoxes = new ArrayList<MatOfPoint>();
       // listOfBoxes.add(box);
       // Imgproc.drawContours(mRgba, listOfBoxes, -1, CONTOUR_COLOR);
        Core.rectangle(mRgba, boxStartPos, boxEndPos, CONTOUR_COLOR,4);
       // if (mIsColorSelected) {
            //mDetector.convexHullProcess(mRgba);
       //  	mDetector.process(mRgba);
       //     List<MatOfPoint> contours = mDetector.getContours();
           
       //    if(contours.size()==0){return mRgba;}
      //      Imgproc.drawContours(mRgba, contours, 0, CONTOUR_COLOR);
            
            if(mIsImageSelected){
            	mIsImageSelected = false;
            	Mat textMat = mRgba.submat(boxStartY, boxEndY, boxStartX, boxEndX);
            	Log.d("fingertip_pos","corp image:"+boxStartY+", "+boxEndY+","+boxStartX+", "+boxEndX);
            	
              Bitmap tempImg = Bitmap.createBitmap(textMat.cols(),  textMat.rows(),Bitmap.Config.ARGB_8888);
              Utils.matToBitmap(textMat, tempImg);  
              saveAsImg(tempImg, "/storage/emulated/0/Pictures/tesseract/tessdata/", "croppedImg.png");
            	
              doOCR(tempImg);
            	/*double maxContourArea = 0;
            	MatOfPoint maxContour = contours.get(0);
            	int maxAreaIdx = 0; 
            	 for (int idx = 0; idx < contours.size(); idx++) 
                 {
                       MatOfPoint contour = contours.get(idx);
                       double contourarea = Imgproc.contourArea(contour);
                       if (contourarea >= maxContourArea) 
                       {
                                maxContour = contour;
                                maxContourArea = contourarea;
                                maxAreaIdx = idx;
                       }
                 }

                 org.opencv.core.Point[] pointsOfContour = maxContour.toArray();
                 int nbPoints = pointsOfContour.length; 
                 double tipPosX = 0;
                 double tipPosY = 360;
                 for(int i=0; i< nbPoints;i++)
                 {
                     if(pointsOfContour[i].y<= tipPosY &&
                    		 pointsOfContour[i].y>=32 &&
                    		 pointsOfContour[i].x<=576 &&
                    		 pointsOfContour[i].x>=64){
                    	 tipPosY = pointsOfContour[i].y;
                    	 tipPosX = pointsOfContour[i].x;
                     }
                	 Log.d("fingertip_pos",""+pointsOfContour[i]);
                 }
                 Log.d("fingertip_pos","tip_pos:"+tipPosX+", "+tipPosY+" size_x:"+mRgba.cols()+" size_y:"+mRgba.rows());
            	mIsImageSelected = false;
            	int endRow = (int) tipPosY+10;
            	if(endRow >= mRgba.rows()){
            		endRow = mRgba.rows();
            	}
            	int startRow = (int) tipPosY-64;
            	if(startRow <= 0){
            		startRow = 0;
            	}
            	int endCol = (int) tipPosX+375;
            	if(endCol >= mRgba.cols()){
            		endCol = mRgba.cols();
            	}
            	int startCol = (int) tipPosX-75;
            	if(startCol <= 0){
            		startCol = 0;
            	}*/           	
//            	outputImgs(textMat);
            	//textMat.setTo(mBlobColorRgba);
            	//mRgba.copyTo(textMat);
            	
            }
           
          

           // Mat spectrumLabel = mRgba.submat(4, 4 + mSpectrum.rows(), 70, 70 + mSpectrum.cols());
           // mSpectrum.copyTo(spectrumLabel);
        //}
            return mRgba;
       
    }
	public void outputImgs( Mat tempMat){
        //Log.i("SAVE IMAGE", "start save");
		File newFile = null;
    	try {
			newFile =File.createTempFile("IMGcapture_0"+index, ".PNG", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
			index++;
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	try {          
	        	Log.d("fingertip_pos", newFile.getAbsolutePath());
	            FileOutputStream fos = new FileOutputStream(newFile);
	            Bitmap tempImg = Bitmap.createBitmap(tempMat.cols(),  tempMat.rows(),Bitmap.Config.ARGB_8888);
	            Utils.matToBitmap(tempMat, tempImg);  
	            //
	            doOCR(tempImg);
	            
	            tempImg.compress(Bitmap.CompressFormat.PNG, 100, fos);
	            fos.flush();
	            fos.close();
	        } catch (IOException e) {
	            Log.e("error", e.getMessage());
	            e.printStackTrace();
	        }
	}
	
	private void saveAsImg(Bitmap newBmp, String imgFilePath, String imgName) {
	  
	  Log.d(TAG, "Saving image, imgFilePath: " + imgFilePath + ", imgName: " + imgName);
    //create a file to write bitmap data
    File f = new File(imgFilePath + imgName);
    try {
      f.createNewFile();

      if (f.exists()) {
        Log.d(TAG, "File created for image.");
      } else {
        Log.d(TAG, "Unable to create file object.");
      }
      //Convert bitmap to byte array
      Bitmap bitmap = newBmp;
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
      byte[] bitmapdata = bos.toByteArray();
      Log.d(TAG, "bitmap byte[] data: " + bitmapdata);

      //write the bytes in file
      FileOutputStream fos = new FileOutputStream(f);
      fos.write(bitmapdata);
      Log.d(TAG, "Writing bitmap data to file completed.");
    } catch (IOException e) {
      Log.e(TAG, "Exception occured while writng image to a file (" + imgFilePath + imgName + ")");
      e.printStackTrace();
    }
  }
	
    private void doOCR(Bitmap bitmap) {
      String DATA_PATH = "/storage/emulated/0/Pictures/tesseract";
      TessBaseAPI baseApi = new TessBaseAPI();
      baseApi.setDebug(true); 
      Log.d(TAG, "Initializing baseApi..., DATA_PATH= " + DATA_PATH);
      baseApi.init(DATA_PATH, "eng");
      baseApi.setImage(bitmap);
      Log.d(TAG, "Decoding text from images... ");
      String recognizedText = baseApi.getUTF8Text();
      Log.d(TAG, "recognizedText: " + recognizedText);
      
      ////////////////////
      if (mOpenCvCameraView != null){
        mOpenCvCameraView.releaseCamera();
      }
      // start InterpretWord Activity
      Intent i = new Intent(this, InterpretWordActivity.class);
      i.putExtra("recognizedText", recognizedText);
      i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(i);
  }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }
    private GestureDetector createGestureDetector(Context context) {
        GestureDetector gestureDetector = new GestureDetector(context);
            //Create a base listener for generic gestures
            gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
                @Override
                public boolean onGesture(Gesture gesture) {
                    if (gesture == Gesture.TAP) {
                    	
                      Log.d(TAG, "Gesture.TAP");
                    //open the menu
                      openOptionsMenu();
                        return true;
                    } else if (gesture == Gesture.TWO_TAP) {
                        // do something on two finger tap
                        mIsImageSelected = true;
                      Log.d(TAG, "Gesture.TWO_TAP");
                        return true;
                    } else if (gesture == Gesture.SWIPE_RIGHT) {
                    	boxStartX = boxStartX-16;
                    	boxEndX = boxEndX+16;
                    	boxStartPos = new org.opencv.core.Point(boxStartX,boxStartY);
                    	boxEndPos = new org.opencv.core.Point(boxEndX,boxEndY);
                      Log.d(TAG, "Gesture.SWIPE_RIGHT");
                        return true;
                    } else if (gesture == Gesture.SWIPE_LEFT) {
                    	boxStartX = boxStartX+16;
                    	boxEndX = boxEndX-16;
                    	boxStartPos = new org.opencv.core.Point(boxStartX,boxStartY);
                    	boxEndPos = new org.opencv.core.Point(boxEndX,boxEndY);
                      Log.d(TAG, "Gesture.SWIPE_LEFT");
                        return true;
                    }else if (gesture == Gesture.SWIPE_UP) {
                    	boxStartY = boxStartY-16;
                    	boxEndY = boxEndY+16;
                    	boxStartPos = new org.opencv.core.Point(boxStartX,boxStartY);
                    	boxEndPos = new org.opencv.core.Point(boxEndX,boxEndY);
                      Log.d(TAG, "Gesture.SWIPE_LEFT");
                        return true;
                    }else if (gesture == Gesture.SWIPE_DOWN) {
                    	boxStartY = boxStartY+16;
                    	boxEndY = boxEndY-16;
                    	boxStartPos = new org.opencv.core.Point(boxStartX,boxStartY);
                    	boxEndPos = new org.opencv.core.Point(boxEndX,boxEndY);
                      Log.d(TAG, "Gesture.SWIPE_LEFT");
                        return true;
                    }
                    return false;
                }

            });
            gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
                @Override
                public void onFingerCountChanged(int previousCount, int currentCount) {
                  // do something on finger count changes
                  Log.d(TAG, "onFingerCountChanged(), previousCount: " + previousCount 
                      + ", currentCount: " + currentCount);
                }
            });
            gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {
                @Override
                public boolean onScroll(float displacement, float delta, float velocity) {
                    // do something on scrolling
                  Log.d(TAG, "onScroll(), displacement: " + displacement 
                      + ", delta: " + delta + ", velocity: " + velocity);
                  return true;
                }
            });
            return gestureDetector;
        }

        /*
         * Send generic motion events to the gesture detector
         */
        @Override
        public boolean onGenericMotionEvent(MotionEvent event) {
            if (mGestureDetector != null) {
                return mGestureDetector.onMotionEvent(event);
            }
            return false;
        }
}